// Esempio 01: accendi il led appena è premuto il pulsante  

// Definisco tutti i led connessi alla scheda
#define LED1 13             
#define LED2                
#define LED3                 
#define LED4                
#define LED5
#define LED6                
#define LED7                
#define LED8                
#define LED9             

// Definisco tutti i bottoni di input
#define BUTTON1 7            
#define BUTTON2              
#define BUTTON3              
#define BUTTON4              
#define BUTTON5              
#define BUTTON6              
#define BUTTON7              
#define BUTTON8              
#define BUTTON9              

#define LEDP1                // LED indicante il turno del giocatore 1 
#define LEDP2                // LED indicante il turno del giocatore 2 

#define RESET 6              // Pulsante di reset
  
void setup() {  
  // Imposto tutti i led connessi in output
  pinMode(LED1, OUTPUT);       
  pinMode(LED2, OUTPUT);       
  pinMode(LED3, OUTPUT);       
  pinMode(LED4, OUTPUT);       
  pinMode(LED5, OUTPUT);       
  pinMode(LED6, OUTPUT);        
  pinMode(LED7, OUTPUT);         
  pinMode(LED8, OUTPUT);        
  pinMode(LED9, OUTPUT);        

  // imposto tutti i led connessi in input
  pinMode(BUTTON1, INPUT);     
  pinMode(BUTTON2, INPUT);     
  pinMode(BUTTON3, INPUT);     
  pinMode(BUTTON4, INPUT);     
  pinMode(BUTTON5, INPUT);     
  pinMode(BUTTON6, INPUT);     
  pinMode(BUTTON7, INPUT);     
  pinMode(BUTTON8, INPUT);     
  pinMode(BUTTON9, INPUT);     

  // Imposto in output i led di segnalzione
  pinMode(LEDP1, OUTPUT);
  pinMode(LEDP2, OUTPUT);
  
  pinMode(RESET, INPUT);      // imposta il pin digitale come input

  // Varibili di lettura input dei bottoni
  int sceltaB1 = 0;          
  int sceltaB2 = 0;          
  int sceltaB3 = 0;          
  int sceltaB4 = 0;          
  int sceltaB5 = 0;          
  int sceltaB6 = 0;          
  int sceltaB7 = 0;          
  int sceltaB8 = 0;          
  int sceltaB3 = 9;          
}  

void loop() {  
  do
  {
    sceltaB1 = digitalRead(BUTTON1);  // legge il valore dell'input e lo conserva  
    if (sceltaB1 == HIGH) {  
      digitalWrite(LED1, HIGH);  //accende il led  
  }
  } while();
  
  
  
  
  // controlla che l'input sia HIGH (pulsante premuto)  
  if (val == HIGH) {  
    digitalWrite(LED, HIGH);  //accende il led  
  }  
}
